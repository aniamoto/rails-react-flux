module.exports = {
    entry: "./app/assets/javascripts/main.jsx",
    output: {
        path: __dirname,
        filename: "app/assets/javascripts/bundle.js"
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    module: {
        loaders: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          loader: 'babel-loader', 
          query: {
            cacheDirectory: true,
            presets: ['es2015', 'react']
          }
        }
      ]
    }  
};