import React from 'react';

import TweetActions from "../actions/TweetActions";

export default class TweetBox extends React.Component {
  onTweetSubmit() {
    event.preventDefault();
    TweetActions.sendTweet(this.refs.tweetTextArea.value);
    this.refs.tweetTextArea.value = '';
  }
  render() {
    return (
      <div className="row">
	    <form onSubmit={this.onTweetSubmit.bind(this)}>
	      <div className="input-field">
	        <textarea ref="tweetTextArea" className="materialize-textarea" />
		    <label>What are you up to?</label>
		    <button type="submit" className="btn right">Send</button>
		  </div>
	    </form>
	  </div>
	);
  }
}