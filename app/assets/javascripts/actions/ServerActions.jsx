import AppDispatcher from "../dispatcher"
import ActionTypes from "../constants"

export default {
  receivedTweets(rawTweets) {
  	console.log(3, "ServerActions.receivedTweets");
  	AppDispatcher.dispatch({
  	  actionType: ActionTypes.RECEIVED_TWEETS,
  	  //rawTweets: rawTweets
  	  // ==
  	  rawTweets
  	})
  },
  receivedATweet(rawTweet) {
  	AppDispatcher.dispatch({
  	  actionType: ActionTypes.RECEIVED_A_TWEET,
  	  rawTweet
  	})
  },
  receivedUsers(rawUsers) {
    console.log(3, "ServerActions.receivedUsers");
    AppDispatcher.dispatch({
      actionType: ActionTypes.RECEIVED_USERS,
      rawUsers
    })
  },
  receivedAFollower(rawFollower) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.RECEIVED_A_FOLLOWER,
      rawFollower
    })
  }
}